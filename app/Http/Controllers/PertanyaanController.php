<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class PertanyaanController extends Controller
{
    public function create(){
        return view('pertanyaan.create');
    }

    public function store(Request $request){
        //code validasi
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi'   => 'required'
        ]);
        //code input to database
        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["judul"],
            "isi"   => $request["isi"]
            ]);

            return redirect('pertanyaan')->with('success', 'Pertanyaan Berhasil Di Tambahkan!');
    }

    public function index(){
        //code untuk menampilkan list data dari database
        $pertanyaan = DB::table('pertanyaan')->get();//select * from pertanyaan
        return view('pertanyaan.index', compact('pertanyaan'));
    }

    public function show($id){
        //code tampilin 1 data by id nya
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.show', compact('pertanyaan'));
    }

    public function edit($id){
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.edit', compact('pertanyaan'));
    }

    public function update($id, Request $request){
                //code validasi
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi'   => 'required'
        ]);

        $query = DB::table('pertanyaan')
                    ->where('id', $id)
                    ->update([
                        'judul' => $request['judul'],
                        'isi'   => $request['isi']
                    ]);
        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Di Rubah!');
    }

    public function destroy($id){
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Di Hapus!');
    }
}
