@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3 mr-3">
    <div class="card card-danger">
        <div class="card-header">
        <h3 class="card-title">Membuat Baru Pertanyaan</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/pertanyaan" method="POST">
            @csrf <!--Token-->
        <div class="card-body">
            <div class="form-group">
            <label for="judul">Judul Pertanyaan</label>
            <input type="judul" class="form-control" id="judul" name="judul" value="{{ old('judul', '') }}" placeholder="Silakan Masukan Judul Pertanyaan">
            @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
            <div class="form-group">
            <label for="isi">Isi Pertanyaan</label>
            <input type="isi" class="form-control" id="isi" name="isi" value="{{old('isi', '')}}" placeholder="Silakan Masukan Isi Pertanyaan">
            @error('isi')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror    
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-danger">Tambah</button>
        </div>
        </form>
    </div>    
</div>

@endsection