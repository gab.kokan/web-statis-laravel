@extends('adminlte.master')

@section('content')
    <div class="mt-3 ml-3 mr-3">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Pertanyaan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                @if (session('success'))
                    <div class="alert alert-success">
                        {{session('success')}}
                    </div>
                @endif
                <a class="btn btn-danger mb-3" href="/pertanyaan/create">Tambah Pertanyaan</a>
              <table class="table table-bordered">
                <thead>                  
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Judul</th>
                    <th>Isi</th>
                    <th style="width: 40px">Actions</th>
                  </tr>
                </thead>
                <tbody>
                    <!--looping untuk mengambil data-->
                    @forelse ($pertanyaan as $key => $post)
                        <tr>
                            <td> {{ $key + 1 }} </td>
                            <td> {{ $post->judul }} </td>
                            <td> {{ $post->isi }} </td>
                            <td style="display: flex;">
                                <a href="/pertanyaan/{{$post->id}}" class="btn btn-info btn-sm mr-1">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <a href="/pertanyaan/{{$post->id}}/edit" class="btn btn-secondary btn-sm mr-1">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <form action="/pertanyaan/{{$post->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn-sm">
                                        <i class="fa fa-trash"></i> 
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="4" align="center">Data Kosong</td>    
                        </tr>
                    @endforelse                  
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
            <!--paginantion            
            <div class="card-footer clearfix">
            <ul class="pagination pagination-sm m-0 float-right">
                <li class="page-item"><a class="page-link" href="#">«</a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">»</a></li>
              </ul>
            </div>-->

          </div>
    </div>
@endsection