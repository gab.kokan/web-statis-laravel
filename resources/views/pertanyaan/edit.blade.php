@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3 mr-3">
    <div class="card card-danger">
        <div class="card-header">
        <h3 class="card-title">Edit Pertanyaan Id Ke {{$pertanyaan->id}}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
            @csrf <!--Token-->
            @method('PUT')
        <div class="card-body">
            <div class="form-group">
            <label for="judul">Judul Pertanyaan</label>
            <input type="judul" class="form-control" id="judul" name="judul" value="{{ old('judul', $pertanyaan->judul) }}" placeholder="Silakan Masukan Judul Pertanyaan">
            @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
            <div class="form-group">
            <label for="isi">Isi Pertanyaan</label>
            <input type="isi" class="form-control" id="isi" name="isi" value="{{old('isi', $pertanyaan->isi)}}" placeholder="Silakan Masukan Isi Pertanyaan">
            @error('isi')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror    
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-danger">Edit</button>
            <a href="/pertanyaan" class="btn btn-secondary">Kembali</a>
        </div>
        </form>
    </div>    
</div>

@endsection