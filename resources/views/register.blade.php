<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Register</title>
</head>
<body>
    <h1> Buat Account Baru ! </h1>
			<h2> Sign Up Form </h2>

		<form action="/welcome" method="post">
        @csrf
			<!--Input-->
			<label for="frname"> First name: </label><br><br>
  				<input type="text" id="frname" name="fname"><br><br>
  			<label for="lsname"> Last name: </label><br><br>
  				<input type="text" id="lsname" name="lname"><br><br>
  			
  			<!--Radio Button-->
  			<label for="gender"> Gender: </label><br><br>
  				<input type="radio" id="male" name="gender">
  			<label for="male"> Male </label><br>
  				<input type="radio" id="female" name="gender">
  			<label for="female"> Female </label><br>
  				<input type="radio" id="other" name="gender">
  			<label for="other"> Other </label><br><br>

  			<!--Select Element-->
  			<label> Nationality: </label><br><br>
  				<select>
    				<option> Indonesia </option>
    				<option> Belanda </option>
    				<option> Amerika </option>
    				<option> Jepang </option>
  				</select><br><br>
			
			<!--Checkbox-->
			<label> Language Spoken: </label><br><br>
			 	<input type="checkbox" id="indo" name="indo">
  			<label for="indo"> Indonesia </label><br>
  				<input type="checkbox" id="inggris" name="inggris">
  			<label for="inggris"> Inggris </label><br>
  				<input type="checkbox" id="other2" name="other2">
  			<label for="other2"> Other </label><br><br>

  				
  			<label> Bio: </label><br><br>
				<textarea cols="25" rows="6"></textarea><br><br>
  					
  			<input type="submit" value="Sign Up">
		</form>
</body>
</html>